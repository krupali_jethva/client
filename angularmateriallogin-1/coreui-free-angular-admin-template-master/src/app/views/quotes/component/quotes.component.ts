import { Component,OnInit } from '@angular/core';
import { QuoteServcie } from '../service/quoteServcie';
import { Quote } from '../model/quote';

@Component({
  templateUrl: 'quotes.component.html'
})
export class QuotesComponent implements OnInit{

  constructor(private quoteServcie: QuoteServcie) { 

  }
  quote:Quote=new Quote();

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }
    
  submitted=false;
  submitMessage:string;
  results=false;

  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  ngOnInit() {
    this.submitMessage="";
  }

  addQuotes(){
    this.quoteServcie.addQuotes(this.quote).subscribe(
      (result)=>{console.log("Reslt:- " + result),this.results=true},
      ()=>{this.results=false;}
    );
    console.log("Result Obtained id:- " + this.results);
    console.log("Result Obtained id:- " + this.quote);

    if(!this.results){
      this.submitted=true;
      this.submitMessage="Add Quote Successfully...";
    }else{
      this.submitted=true;
      this.submitMessage="Add Quote Failed...";
    }
    this.quote=new Quote();
  }
}
