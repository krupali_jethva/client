import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Quote } from '../model/quote';
import { QuoteServcie } from '../service/quoteServcie';
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: 'quotedetail',
  templateUrl: './quotesdetail.component.html'
})
export class QuoteDetail implements OnInit {
    quote: Quote;
    id:string;
    quotesList: Quote[] = [];


	constructor(private quoteServcie: QuoteServcie, private location: Location,private route: ActivatedRoute) { }
	ngOnInit() {
        // this.route.paramMap.subscribe(params => {
        //     this.quotesList.forEach((q: Quote) => {
        //       if (q.id == params.id) {
        //         this.quote = q;
        //       }
        //     });
        //   });
        console.log("In Details"+window.sessionStorage.getItem("id"))
		this.getQuoteDetail();
    }
    
	getQuoteDetail(): void {
        const id = + this.route.snapshot.paramMap.get('id');
        console.log(id);
       //this.id=+window.sessionStorage.getItem("id");
    //    this.userService.userList(this.uid).subscribe(
    //      (data)=>this.userList=data,
    //      (error)=>console.log(error),
    //    );
		//this.quoteServcie.getQuoteById(id).subscribe(quote => this.quote = quote);
    } 
	goBack(): void {
		this.location.back();
	}
}