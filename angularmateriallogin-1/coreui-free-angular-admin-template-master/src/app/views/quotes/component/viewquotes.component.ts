import { Component, OnInit } from "@angular/core";
import { Observable } from 'rxjs';
import { QuoteServcie } from '../service/quoteServcie';
import { Quote } from '../model/quote';

@Component({
  selector: "viewquotes",
  templateUrl: "viewquotes.component.html"
})
export class ViewQuotes implements OnInit {
  constructor(private quoteServcie: QuoteServcie) {}

  quotesList: Quote[] = [];
 // quotesList: Observable<Quote[]>;

  ngOnInit() {
    this.reloadData();
  }
  reloadData() {
  //  this.quotesList = this.quoteServcie.getQuotesList();
  this.quoteServcie.getQuotesList().subscribe(quotesList => this.quotesList = quotesList);
  console.log(this.quotesList);
  }
  deleteQuote(id: string): void {
		this.quoteServcie.deleteQuote(id).subscribe(success => {this.reloadData();});
	}
}
