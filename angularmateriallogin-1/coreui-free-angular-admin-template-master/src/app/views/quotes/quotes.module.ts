import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BsDropdownModule, TabsModule, CarouselModule, CollapseModule, PopoverModule, PaginationModule, ProgressbarModule, TooltipModule } from 'ngx-bootstrap';
import { CardsComponent } from '../base/cards.component';
import { FormsComponent } from '../base/forms.component';
import { SwitchesComponent } from '../base/switches.component';
import { PaginationsComponent } from '../base/paginations.component';
import { NavbarsComponent } from '../base/navbars/navbars.component';
import { TooltipsComponent } from '../base/tooltips.component';
import { ProgressComponent } from '../base/progress.component';
import { PopoversComponent } from '../base/popovers.component';
import { CollapsesComponent } from '../base/collapses.component';
import { CarouselsComponent } from '../base/carousels.component';
import { TabsComponent } from '../base/tabs.component';
import { TablesComponent } from '../base/tables.component';
import { QuotesRoutingModule } from './quotes-routing.module';
import { QuotesComponent } from './component/quotes.component';
import { QuoteServcie } from './service/quoteServcie';
import { ViewQuotes } from './component/viewquotes.component';
import { QuoteDetail } from './component/quotesdetail.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    QuotesRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule,
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    QuotesComponent ,ViewQuotes,QuoteDetail   
  ],
  providers:[QuoteServcie]

})
export class QuotesModule { }