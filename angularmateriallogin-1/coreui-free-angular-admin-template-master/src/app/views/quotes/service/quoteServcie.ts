import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Quote } from '../model/quote';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class QuoteServcie {

  private baseUrl = 'http://localhost:8080/quote/';

  private getQuotes: Quote[] = new Array();
  constructor(private httpClient: HttpClient) { }

  addQuotes(quote: Quote): Observable<Quote> {
    quote.quoteContent="test";
    return this.httpClient.post<Quote>(`${this.baseUrl}` + 'add-quote', quote);
  }
  getQuotesList(): Observable<Quote[]> {
    return this.httpClient.get<Quote[]>(`${this.baseUrl}` + 'get-all-quote');
  }

  deleteQuote(id: string): Observable<any> {
    return this.httpClient.delete(`${this.baseUrl}${id}`, { responseType: 'text' });
  }

  getQuoteById(id: string): Observable<any>{
    console.log("----quotedetail----");
    return this.httpClient.get<Quote>(`${this.baseUrl}quotedetail/${id}`);
  }
}