export class Quote {
    id: string;
    appId: number;   
    quoteContent: string;
    siteUrl:string;
    metaKeywords:string;
    pageCount:string;
    quoteSelector:string;
    autherName:string;
    quoteMaxChars:string;
    updateBy:string;
    createBy:string;

    constructor(id?: string, quoteContent?: string, siteUrl?: string, autherName?: string) {
      this.id = id;
      this.quoteContent = quoteContent;
      this.siteUrl = siteUrl;
      this.autherName = autherName;
    }

  }