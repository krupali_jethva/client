import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuotesComponent } from './component/quotes.component';
import { ViewQuotes } from './component/viewquotes.component';
import { QuoteDetail } from './component/quotesdetail.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Quotes'
    },
    children: [         
      {
        path: 'quotesadd',
        component: QuotesComponent,
        data: {
          title: 'QuotesAdd'
        }
      },
      {
        path: 'viewquotes',       
        children: [
          {
          path: '',
          component: ViewQuotes,
          data: {
            title: 'viewquotes'
          }
          },
          { 
          path: 'quotedetail/:id',
          component: QuoteDetail,
          data:{
              title:'quotedetail'
          }
        }
        ]
      },
      { path: 'edit/:id', component: QuotesComponent },     
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuotesRoutingModule {}
