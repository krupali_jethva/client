import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRegisterComponent } from './components/user-register/user-register.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { RoleListComponent } from './components/role-list/role-list.component';


const routes: Routes = [
  {path: '', redirectTo:'login-user' ,pathMatch:'full'},
  {path: 'login-user', component: UserLoginComponent},
  {path: 'userEdit/:userId',component: UserListComponent},
  {path: 'register-user', component: UserRegisterComponent},
  {path: 'user-list', component: UserListComponent},
  {path: 'role-list', component: RoleListComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
