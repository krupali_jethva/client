import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { ActivatedRoute, Router } from '@angular/router';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  submitted=false;
  submitMessage:string;
  results=false;
  user:User=new User();

  constructor(private route: ActivatedRoute, private router: Router,private userServcie: UserServiceService) { }

  ngOnInit() {
    this.submitted=false;
    this.submitMessage="";
  }

  register(){
    this.userServcie.registerUser(this.user).subscribe(
      (result)=>{console.log("Reslt:- " + result),this.results=true},
      ()=>{this.results=false;}
    );
    console.log("Result Obtained id:- " + this.results);
    if(!this.results){
      this.submitted=true;
      this.submitMessage="User Registered Successfully...";
    }else{
      this.submitted=true;
      this.submitMessage="User Registration Failed...";
    }
    this.user=new User();
  }
}