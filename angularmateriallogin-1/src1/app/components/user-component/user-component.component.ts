import { Component, OnInit } from '@angular/core';
import { SharedServceService } from 'src/app/service/shared-servce.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-component',
  templateUrl: './user-component.component.html',
  styleUrls: ['./user-component.component.css']
})
export class UserComponentComponent implements OnInit {

  loginResults=true;
  message:string="";

  constructor(private data:SharedServceService,private route:ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.data.currentValue.subscribe(result=> this.loginResults=result);
  }

  logout(){
    window.sessionStorage.removeItem("userId");
    this.loginResults=true;
    this.router.navigate(['login-user']);
  }
}
