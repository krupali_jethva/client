import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserServiceService } from 'src/app/service/user-service.service';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userList:User[];
  uid:number;
  isEdit=false;
  user:User=new User();

  constructor(private route:ActivatedRoute,private userService:UserServiceService,private router:Router) { }

  ngOnInit() {
    if(window.sessionStorage.getItem("userId")==null){
      this.router.navigate(['login-user'])
    }
    this.uid=+window.sessionStorage.getItem("userId");
    this.userService.userList(this.uid).subscribe(
      (data)=>this.userList=data,
      (error)=>console.log(error),
    );
  }

  editUser(userId:number){
    this.userService.userEdit(userId).subscribe(
      (dataResult)=>this.user=dataResult,
      (error)=>console.log(error),
    );
    this.isEdit=true;
  }

  cancelEditUser(){
    this.isEdit=false;
    this.ngOnInit();
  }

  updateUser(){
    this.userService.userUpdate(this.user).subscribe(
      (dataResult)=>{
        if(dataResult==true){
          this.isEdit=false,
          this.ngOnInit()
        }
      },
      (error)=>console.log(error),
    );
  }

  deleteUser(userId:number){
    if(confirm("Are you sure you want to delete?")){
      this.userService.userDelete(userId).subscribe(
        (result)=>{
          if(result==true){
            this.isEdit=false,
            this.ngOnInit()
          }else{
            console.log("failed to delete")
          }
        },
        (error)=>console.log(error),
      );
    }else{
      this.isEdit=false;
      this.ngOnInit();
    }
  }
}