import { Component, OnInit } from '@angular/core';
import { Activity } from 'src/app/model/activity';
import { ActivatedRoute, Router } from '@angular/router';
import { UserServiceService } from 'src/app/service/user-service.service';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {

  activityList:Activity[];
  activity:Activity=new Activity();
  user:User=new User();
  userId:number;
  dataMsgResult=false;
  isAdd=true;

  constructor(private route:ActivatedRoute,private router:Router,private userService:UserServiceService) { }

  ngOnInit() {
    if(window.sessionStorage.getItem("userId")==null){
      this.router.navigate(['login-user'])
    }
    this.userId=+window.sessionStorage.getItem("userId");
    this.user.userId=this.userId;
    this.activity.userModel=this.user;
    this.userService.activityList(this.userId).subscribe(
      (data)=>this.activityList=data
    );
  }

  addActivity(){
    this.userService.activityAdd(this.activity).subscribe(
      (dataResult) => this.ngOnInit(),
      (error)=>console.log(error),
    );
    this.activity.activityName="";
    this.ngOnInit();
  }

  editActivity(activityId:number){
    this.isAdd=false;
    this.userService.activityEdit(activityId).subscribe(
      (activityResult)=>this.activity=activityResult,
      (error)=>console.log(error),
    );
  }

  cancelEdit(){
    this.isAdd=true;
    this.activity.activityName="";
  }

  updateActivity(){
    this.userService.activityUpdate(this.activity).subscribe(
      (data)=> this.ngOnInit(),
      (error)=>console.log(error),
    );
    this.isAdd=true;
    this.activity.activityName="";
  }

  deleteActivity(activityId:number){
    this.userService.activityDelete(activityId).subscribe(
      (result)=>{
        if(result==true){
          this.ngOnInit()
        }
      },
      (error)=>console.log(error),
    );
  }
}
