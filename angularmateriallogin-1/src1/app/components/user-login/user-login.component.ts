import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserServiceService } from 'src/app/service/user-service.service';
import { User } from 'src/app/model/user';
import { SharedServceService } from 'src/app/service/shared-servce.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  user:User=new User();
  loginResult=false;
  loginMessage="";

  constructor(private route: ActivatedRoute, private router: Router,private userServcie: UserServiceService,private data: SharedServceService) { }

  ngOnInit() {
    window.sessionStorage.removeItem("userId");
  }

  login(){
    if(window.sessionStorage.getItem("userId")==null){
    this.userServcie.loginUser(this.user).subscribe(
      (result)=>{
        console.log(result);
        if(result.length==0){
          this.loginResult=true;
          this.loginMessage="Invalid User Credentials"; 
          window.sessionStorage.removeItem("userId");
          this.data.changeValue(true);
        }else{
          this.loginResult=true;
          this.loginMessage="Successfully logged in...";
          window.sessionStorage.setItem("userId",""+result[0].userId);
          this.data.changeValue(false);
          this.router.navigate(['user-list']);
        }
      },
      ()=>{
        this.loginResult=true;
        window.sessionStorage.removeItem("userId");
        this.loginMessage="Invalid User Credentials";
        this.data.changeValue(true);
      }
    );
  }else{
    this.loginResult=true;
    this.loginMessage="User already logged in...";
    this.data.changeValue(true);
  }
  this.user.userEmail="";
  this.user.userPassword="";
  }

  callLive(){
    this.userServcie.feedBackList().subscribe(
      (result)=>console.log(result),
      (error)=>console.log(error),
    );
  }
}
