import { User } from './user';

export class Activity {

    activityId:number;
    activityName:string;
    userModel:User;
}
