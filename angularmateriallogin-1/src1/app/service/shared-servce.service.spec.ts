import { TestBed } from '@angular/core/testing';

import { SharedServceService } from './shared-servce.service';

describe('SharedServceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedServceService = TestBed.get(SharedServceService);
    expect(service).toBeTruthy();
  });
});
