import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { Activity } from '../model/activity';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  private baseUrl = 'http://localhost:8080/user/';

  constructor(private httpClient:HttpClient) { }

  registerUser(user:User):Observable<User>{
    return this.httpClient.post<User>(`${ this.baseUrl }`+'register-user',user);
  }

  loginUser(user:User):Observable<User[]>{
    return this.httpClient.post<User[]>(`${ this.baseUrl}`+'login-user',user);
  }

  userList(userId:number):Observable<User[]>{
    return this.httpClient.get<User[]>(`${ this.baseUrl}`+'getUserList?uid='+userId);
  }

  activityList(userId:number):Observable<Activity[]>{
    return this.httpClient.get<Activity[]>(`${ this.baseUrl}`+'activity-list?uid='+userId);
  }

  activityAdd(activity:Activity):Observable<Activity[]>{
    return this.httpClient.post<Activity[]>(`${ this.baseUrl}`+'activity-add',activity);
  }

  activityEdit(activityId:number):Observable<Activity>{
    return this.httpClient.get<Activity>(`${ this.baseUrl}`+'activity-edit?activityId='+activityId);
  }

  activityUpdate(activity:Activity):Observable<Activity>{
    return this.httpClient.post<Activity>(`${ this.baseUrl}`+'activity-update',activity);
  }

  activityDelete(activityId:number):Observable<boolean>{
    return this.httpClient.get<boolean>(`${ this.baseUrl}`+'activity-delete?activityId='+activityId);
  }

  userEdit(userId:number):Observable<User>{
    return this.httpClient.get<User>(`${ this.baseUrl}`+'user-edit?uid='+userId);
  }

  userUpdate(user:User):Observable<boolean>{
    return this.httpClient.post<boolean>(`${ this.baseUrl }`+'user-update',user);
  }

  userDelete(userId:number):Observable<boolean>{
    return this.httpClient.get<boolean>(`${ this.baseUrl}`+'user-delete?uid='+userId);
  }

  feedBackList():Observable<any>{
    return this.httpClient.get<any>('http://localhost:8080/ws/getFeedBackCategoryList');
  }
}