import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedServceService {

  private resultVal = new BehaviorSubject(true);
  currentValue = this.resultVal.asObservable();

  constructor() { }

  changeValue(value: boolean) {
    this.resultVal.next(value)
  }
}