<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<style type="text/css">
.button-register {
	background-color: green;
	color: white;
}

.button-report {
	background-color: #000000;
	color: white;
	margin-left: 30%;
}
</style>
<title>Admin Page</title>
</head>
<body>
	<!-- <h2>Struts 2 Create, Read, Update and Delete (CRUD) Example using
		JDBC</h2> -->
	<s:form action="userloginLogin.action">
		<s:textfield name="employee.userName" label="User Name"></s:textfield>
		<s:password name="employee.userPassword" label="Password"></s:password>
		<s:submit cssClass="button-register" value="Login" />
	</s:form>
</body>
</html>