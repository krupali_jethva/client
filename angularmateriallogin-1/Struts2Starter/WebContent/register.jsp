<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<style type="text/css">
.button-register {
	background-color: green;
	color: white;
}

.button-report {
	background-color: #000000;
	color: white;
	margin-left: 30%;
}
</style>
<title>Admin Page</title>
</head>
<body>
	<!-- <h2>Struts 2 Create, Read, Update and Delete (CRUD) Example using
		JDBC</h2> -->
	<s:form action="registerEmp.action" method="post">
		<s:textfield label="User Name" name="employee.userName" />
		<s:textfield label="Email" name="employee.userEmail" />
		<s:password label="Password" name="employee.userPassword" />
		<s:select label="Select Role" headerKey="-1" headerValue="Select Role"
			list="#{'1':'admin', '2':'user'}" name="employee.userRole" />
		<s:submit cssClass="button-register" value="Resgister" />
	</s:form>
	<s:if test="ctr>0">
		<span style="color: green;"><s:property value="msg" /></span>
	</s:if>
	<s:else>
		<span style="color: red;"><s:property value="msg" /></span>
	</s:else>
</body>
</html>