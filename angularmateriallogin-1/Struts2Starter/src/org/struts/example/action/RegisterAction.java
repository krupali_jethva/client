package org.struts.example.action;

import java.sql.SQLException;

import org.struts.example.dao.EmployeeDao;
import org.struts.example.dao.EmployeeDaoImpl;
import org.struts.example.model.Employee;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

public class RegisterAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Employee employee;

	private EmployeeDao employeeDao=new EmployeeDaoImpl();

	
/*	public String execute() throws Exception {
		// call Service class to store personBean's state in database
		System.out.println("execute : RegisterAction");
		register();
		return SUCCESS;
	}
*/
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String register() {
		String status = "error";
		System.out.println("employee :: " + employee);	

		if (employeeDao.addEmployee(employee)) {
			status = Action.SUCCESS;
		} else {
			status = Action.ERROR;
		}
		return status;
	}
}
