package org.struts.example.action;

import com.opensymphony.xwork2.ActionSupport;

public class TestAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String execute() {
		System.out.println("Hello This is Struts Starter !!");
		return "success";
	}

	public String admin() {
		return "admin";
	}

	public String user() {
		return "user";
	}

	public String register() {

		return "register";
	}
	public String login()
	{
		return "login";
	}

}
