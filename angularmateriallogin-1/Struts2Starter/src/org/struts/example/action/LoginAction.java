package org.struts.example.action;

import org.struts.example.dao.EmployeeDao;
import org.struts.example.dao.EmployeeDaoImpl;
import org.struts.example.model.Employee;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Employee employee;

	private EmployeeDao employeeDao = new EmployeeDaoImpl();

	public String userlogin() {
		String status = Action.ERROR;
		System.out.println("employee" + employee);
		if (employeeDao.getEmployee(employee)) {
			status = Action.SUCCESS;
		} else {
			status = Action.ERROR;
		}
		System.out.println("status" + status);
		return status;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
