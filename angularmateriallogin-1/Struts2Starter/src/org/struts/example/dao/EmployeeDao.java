package org.struts.example.dao;

import org.struts.example.model.Employee;

public interface EmployeeDao {

	boolean addEmployee(Employee employee);

	boolean getEmployee(Employee employee);
	
	

}
