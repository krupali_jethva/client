package org.struts.example.dao;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.struts.example.model.Employee;
import org.struts.example.util.JdbcConnection;

public class EmployeeDaoImpl implements EmployeeDao {

	private Connection conn = (Connection) JdbcConnection.getConnection();
	private ResultSet resultSet = null;
	private java.sql.Statement stmt = null;
	private Employee emp = new Employee();
	private String sql_query;
	private PreparedStatement pstmt = null;

	@Override
	public boolean addEmployee(Employee employee) {
		boolean status = false;
		if (employee != null) {
			sql_query = "INSERT INTO struts_tutorial.employee(userName,userEmail,userPassword,userRole) VALUES(?,?,?,?)";
			try {
				pstmt = conn.prepareStatement(sql_query);
				pstmt.setString(1, employee.getUserName());
				pstmt.setString(2, employee.getUserEmail());
				pstmt.setString(3, employee.getUserPassword());
				pstmt.setInt(4, employee.getUserRole());

				status = pstmt.execute();

				System.out.println("Value of status in addEmployee = " + status);
			} catch (SQLException e) {
				System.out.println("Problem Occured while inserting Employee");
				e.printStackTrace();
			}
		} else {
			System.out.println("Please provide valid employee to add");
		}
		return !status;
	}

	@Override
	public boolean getEmployee(Employee employee) {
		boolean status = false;
		List<Employee> elist = getAllEmployee();
		if (employee != null) {
			Iterator<Employee> EmployeeIterator = elist.iterator();

			while (EmployeeIterator.hasNext()) {
				emp = EmployeeIterator.next();
				if (employee.getUserName().equals(emp.getUserName())) {
					System.out.println("Username is not Available!!!");
					return false;
				}
			}

			sql_query = "SELECT * FROM struts_tutorial.employee where userName='" + employee.getUserName() + "' and userPassword='"
					+ employee.getUserPassword() + "'";
			System.out.println(sql_query);
			try {
				stmt = conn.createStatement();
				resultSet = stmt.executeQuery(sql_query);

				while (resultSet.next()) {
					emp.setId(resultSet.getInt(1));
					emp.setUserName(resultSet.getString(2));
					emp.setUserEmail(resultSet.getString(3));
					emp.setUserPassword(resultSet.getString(4));
					emp.setUserRole(resultSet.getInt(5));
				}

				if (emp.getUserName().equals(employee.getUserName())
						&& emp.getUserPassword().equals(employee.getUserPassword())) {
					status = true;
				} else {
					status = false;
				}
			} catch (Exception e) {
				System.out.println("Problem Occured while getting logincheck = " + e.getMessage());
			}
		}
		return status;
	}

	private List<Employee> getAllEmployee() {
		List<Employee> emplist = new ArrayList<Employee>();
		sql_query = "SELECT * FROM struts_tutorial.employee";
		try {

			stmt = conn.createStatement();
			resultSet = stmt.executeQuery(sql_query);

			while (resultSet.next()) {
				emp.setId(resultSet.getInt(1));
				emp.setUserName(resultSet.getString(2));
				emp.setUserEmail(resultSet.getString(3));
				emp.setUserPassword(resultSet.getString(4));
				emp.setUserRole(resultSet.getInt(5));
				/*
				 * if(!emp.equals(null)){ System.out.println("Name of employee @Addding : " +
				 * emp.getEname()); }
				 */
				emplist.add(emp);
			}
		} catch (Exception ex) {
			System.out.println("Problem Occured while getting All Employee");
			ex.printStackTrace();
		}
		return emplist;
	}

}
