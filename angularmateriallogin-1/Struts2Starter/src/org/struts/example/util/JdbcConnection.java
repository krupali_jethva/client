package org.struts.example.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class JdbcConnection {

	private static Connection conn = null;

	public static Connection getConnection() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/struts_tutorial", "root", "root");
		} catch (Exception ex) {
			System.out.println("Problem Occured while getting connection to the Database = " + ex.getMessage());
			// ex.printStackTrace();
		}
		return conn;
	}

	public static void CloseConnection() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception ex) {
			System.out.println("Problem Occured while Closing the connection to the Database");
		}
	}
}
