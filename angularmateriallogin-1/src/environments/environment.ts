// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// 2. Add your credentials from step 1
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAgTkje1rnQIH9_7_GZo0IPoVtJuNxXOyU",
    authDomain: "angularcms-7cb3d.firebaseapp.com",
    databaseURL: "https://angularcms-7cb3d.firebaseio.com",
    projectId: "angularcms-7cb3d",
    storageBucket: "angularcms-7cb3d.appspot.com",
    messagingSenderId: "302046619079",
    appId: "1:302046619079:web:21fdc317adea67a498a5e2",
    measurementId: "G-882BR96F75"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
