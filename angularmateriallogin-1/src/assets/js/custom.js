function myTest(arr) {
    console.log("item "+ JSON.stringify(arr));  
    //alert('Welcome to custom js');
    CKEDITOR.replace( 'txtEditor', {
        toolbarGroups: [
            { name: 'mode' },
            { name: 'basicstyles' }
        ],    
        on: {
            pluginsLoaded: function() {
                var editor = this,
                    config = editor.config;                
                editor.ui.addRichCombo( 'myComboControls', {
                    label: 'Controls',
                    title: 'Controls',
                    toolbar: 'basicstyles,0',            
                    panel: {               
                        css: [ CKEDITOR.skin.getPath( 'editor' ) ].concat( config.contentsCss ),
                        multiSelect: false,
                        attributes: { 'aria-label': 'Controls' }
                    },
                    init: function() { 
                        for (var i = 0; i < arr.length; i++) {
                                this.add(arr[i].id,arr[i].roleName);
                        } 
                    },        
                    onClick: function( value ) {
                        editor.focus();
                        editor.fire( 'saveSnapshot' );                   
                        editor.insertHtml( value );                
                        editor.fire( 'saveSnapshot' );
                    }
                } ); 
            }        
        }
    } );
}
const GITHUB_CLIENT_ID = "eda7516dae83680ce74e" // or get from process.env.GITHUB_CLIENT_ID
const GITHUB_CLIENT_SECRET = "043fceee6c0bd18279f654db31b734145072f831" // or get from process.env.GITHUB_CLIENT_SECRET
const GITHUB_CALLBACK_URL = "http://localhost:4200/" // or get from process.env.GITHUB_CALLBACK_URL
function githubSignInJS(){
    alert("github");
    // Initialize with your OAuth.io app public key
    OAuth.initialize(GITHUB_CLIENT_ID);
    // Use popup for oauth
    // Alternative is redirect
    OAuth.popup('github').then(github => {
      console.log('github:', github);
      // Retrieves user data from oauth provider
      // Prompts 'welcome' message with User's email on successful login
      // #me() is a convenient method to retrieve user data without requiring you
      // to know which OAuth provider url to call
      github.me().then(data => {
        console.log('me data:', data);
        alert('GitHub says your email is:' + data.email + ".\nView browser 'Console Log' for more details");
  });
      // Retrieves user data from OAuth provider by using #get() and
      // OAuth provider url
      github.get('/user').then(data => {
        console.log('self data:', data);
      })
    });
}

$(function() {
   console.log('Hello, custom js');
});