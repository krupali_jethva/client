import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { AddRole } from '../model/add-role';
import { HttpClient } from '@angular/common/http';
import { UserStatusMaster } from '../model/status-master';
import { CaptchaModel } from '../model/captcha-model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
 

  private baseUrl: string;

  constructor(private http: HttpClient) {
  this.baseUrl = 'http://localhost:8080/admin/';
  }

  saveRole(AddRole: AddRole): Observable<AddRole> {
  // console.log(this.http.post(this.baseUrl, AddRole));
    return this.http.post<AddRole>(this.baseUrl + "saveRole", AddRole);
  }
  getRoleList(): Observable<AddRole[]> {
    return this.http.get<AddRole[]>(this.baseUrl+"roles");
  }
  roleEdit(roleId:number):Observable<AddRole>{
    return this.http.get<AddRole>(this.baseUrl+'roleEdit/'+roleId);
  }

  roleUpdate(AddRole:AddRole):Observable<AddRole>{  
    console.log("id:"+AddRole.id);
    return this.http.put<AddRole>(this.baseUrl + "updateRole/"+AddRole.id,AddRole);
  }
  roleDelete(roleId:number):Observable<AddRole>{
    return this.http.delete<AddRole>(this.baseUrl+'roleDelete/'+roleId);
  }

  getStatusList():Observable<UserStatusMaster[]> {    
    return this.http.get<UserStatusMaster[]>(this.baseUrl+"status");
  }
  
  getCaptcha():Observable<CaptchaModel>{
    console.log("---> in getCaptcha : -> "+this.http.get<CaptchaModel>(this.baseUrl+"getCaptchaImage"));
    return this.http.get<CaptchaModel>(this.baseUrl+"getCaptchaImage");
  }
}
