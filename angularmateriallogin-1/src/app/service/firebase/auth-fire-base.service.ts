import { Injectable, NgZone } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
@Injectable({
  providedIn: 'root'
})
export class AuthFireBaseService {
  userData: any; // Save logged in user data

  constructor( public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,  
    public ngZone: NgZone) { }

  // Sign in with email/password
  SignIn(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        });
       // this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }
    /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  // SetUserData(user) {
  //   const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.id}`);
  //   // const userData: User = {
  //   //   id: user.id,
  //   //   email: user.email,
  //   //   username: user.username,
  //   //   profileImage: user.profileImage,
      
  //   // }
  //   return userRef.set(userData, {
  //     merge: true
  //   })
  // }
}
