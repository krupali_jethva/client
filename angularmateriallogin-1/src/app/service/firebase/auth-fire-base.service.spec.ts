import { TestBed } from '@angular/core/testing';

import { AuthFireBaseService } from './auth-fire-base.service';

describe('AuthFireBaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthFireBaseService = TestBed.get(AuthFireBaseService);
    expect(service).toBeTruthy();
  });
});
