import { Injectable } from '@angular/core';
import { Socialusers } from '../model/socialusers';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocialloginService {
  private baseUrl: string;

  constructor(private http: HttpClient) {  this.baseUrl = 'http://localhost:8080/admin/'; }
  Savesresponse(Socialusers: Socialusers): Observable<Socialusers> {
    // console.log(this.http.post(this.baseUrl, AddRole));
      return this.http.post<Socialusers>(this.baseUrl + "Login/Savesresponse", Socialusers);
    }

}
