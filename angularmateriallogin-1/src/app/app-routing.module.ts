import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogInComponent } from './components/log-in/log-in.component';
import { RegisterComponent } from './components/register/register.component';
import { UserlistComponent } from './components/userlist/userlist.component';
import { FileuploadComponent } from './components/fileupload/fileupload.component';
import { RoleMasterComponent } from './components/role-master/role-master.component';
import { MaterialComponent } from './components/material/material.component';
import { InitiativeComponent } from './components/initiative/initiative/initiative.component';
import { SocialLoginComponent } from './components/social-login/social-login.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'login', component: LogInComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'users', component: UserlistComponent },
  { path: 'app-fileupload', component: FileuploadComponent },
  {path:'app-role-master',component:RoleMasterComponent},
  {path:'app-mat',component:MaterialComponent},
  {path :'app-initiative',component:InitiativeComponent},
  {path:'app-social-login',component:SocialLoginComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }