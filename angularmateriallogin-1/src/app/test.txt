	CKEDITOR.replace( 'txtEditor', {
    toolbarGroups: [
        { name: 'mode' },
        { name: 'basicstyles' }
    ],    
    on: {
        pluginsLoaded: function() {
            var editor = this,
                config = editor.config;
            
            editor.ui.addRichCombo( 'myComboControls', {
                label: 'Controls',
                title: 'Controls',
                toolbar: 'basicstyles,0',
        
                panel: {               
                    css: [ CKEDITOR.skin.getPath( 'editor' ) ].concat( config.contentsCss ),
                    multiSelect: false,
                    attributes: { 'aria-label': 'Controls' }
                },
                init: function() {    
                    this.add( 'foo', 'Foo!' );
                    this.add( 'bar', 'Bar!' ); 
                    this.add( 'ping', 'Ping!' );
                    this.add( 'pong', 'Pong!' );     
                    this.add( 'bar', 'Bar!' );                    
                    this.add( 'bar', 'Bar!' );                    
                    this.add( 'bar', 'Bar!' );                    
                    this.add( 'bar', 'Bar!' );                    
                    this.add( 'bar', 'Bar!' );                    
                    this.add( 'bar', 'Bar!' );                    
                    this.add( 'bar', 'Bar!' );                    
                    this.add( 'bar', 'Bar!' );                    
                    this.add( 'bar', 'Bar!' );                    
                    this.add( 'bar', 'Bar!' ); 
                },
    
                onClick: function( value ) {
                    editor.focus();
                    editor.fire( 'saveSnapshot' );                   
                    editor.insertHtml( value );                
                    editor.fire( 'saveSnapshot' );
                }
            } ); 
        }        
    }
} );