export class SiteMaster {
    id:number;
    name:String;
    siteUrl:String;
    applicationPath:String;
    physicalPath:String;
    hitCount:number;
}
    