import { SiteMaster } from './site-master';

export class AddRole {
    id: number;
    roleName: string;
    roleDescription: string;
    roleOrder:number;
    isActive:boolean;
    siteId : SiteMaster;  
}
