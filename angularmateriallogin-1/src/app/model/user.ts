import { AddRole } from './add-role';
import { UserStatusMaster } from './status-master';

export class User {
    id: string;
    firstName:String;
    lastName:String;
    username: string;
    email: string;
    password:string;
    confirmPassword:string;
    profileImage : string;  
    captchCode:string;
    roleMaster:number;
    statusMaster:number;
    isSuperAdmin:boolean;
}
