import { BrowserModule } from '@angular/platform-browser';

/* file Upload */

import { FileUploadModule } from "ng2-file-upload";

/* Routing */
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

/* Angular Material */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

/* FormsModule */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Angular Flex Layout */
import { FlexLayoutModule } from "@angular/flex-layout";

/* Components */
import { LogInComponent } from './components/log-in/log-in.component';
import { RegisterComponent } from './components/register/register.component';
import { AppService } from './app.service';
import { HttpClientModule } from '@angular/common/http';
import { UserlistComponent } from './components/userlist/userlist.component';

import { FileuploadComponent } from './components/fileupload/fileupload.component';
import { HttpModule } from '@angular/http';
import { FileService } from './utilService/file.service';
import { RoleMasterComponent } from './components/role-master/role-master.component';
import { UserService } from './service/user.service';
import { MaterialComponent } from './components/material/material.component';
import { InitiativeComponent } from './components/initiative/initiative/initiative.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SocialLoginComponent } from './components/social-login/social-login.component';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, LinkedinLoginProvider, FacebookLoginProvider, AuthService } from 'ng4-social-login';

import { SocialloginService } from './service/sociallogin-service';
import { AuthFireBaseService } from './service/firebase/auth-fire-base.service';

// 1. Import the libs you need
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';

export function socialConfigs() {
  const config = new AuthServiceConfig([{
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('531834157364607')
  }, {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('441016610729-vn126c79ajui4k5n89sc2l0fn4a9rjcj.apps.googleusercontent.com')
  }], false);
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    RegisterComponent,
    RegisterComponent,
    LogInComponent,
    UserlistComponent,
    FileuploadComponent, RoleMasterComponent, MaterialComponent, InitiativeComponent, SocialLoginComponent],
  imports: [
    // 3. Initialize
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule, // storage
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule, FileUploadModule, HttpModule, CKEditorModule, SocialLoginModule

  ],
  providers: [AppService, FileService, UserService, AuthService, SocialloginService, AuthFireBaseService,
    {
      provide: AuthServiceConfig,
      useFactory: socialConfigs
    }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule { }