import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { User } from 'src/app/model/user';
import { ConcatSource } from 'webpack-sources';
import { FileService } from 'src/app/utilService/file.service';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  users: Array<any>;
  constructor(private appService: AppService,private fileService:FileService,private router: Router) { }
  profilePic = null;
  ngOnInit() {
    this.reloadData();
  }
  displayedColumnsName: string[] = ['Id', 'Name','User Name','Email','Role','Status','IsSuperAdmin','Action'];
  userDataSource: MatTableDataSource<User>;

  reloadData() {
    this.appService.getUserList().subscribe(data => {
      this.users = data.json();
      this.userDataSource = new MatTableDataSource(data.json());

      // for (const user of this.users) {
      //   if (user.profileImage == null) {
      //     user.profileImage = 'assets/image/no_profile.png';
      //   }
      //   else {
      //     user.profileImage= 'G:/opt/angularfileupload/' + user.profileImage;
      //  //   this.fileService.getBase64ImageFromURL(imageUrl).subscribe(base64data => {    
      //       //console.log(base64data);
      //       // this is the image as dataUrl
      //       //user.profileImage="";
      //     //  user.profileImage = imageUrl;
      //   //  });      
      //   }
      // }
    });
  }
}

// export class UserDataSource extends DataSource<any>
// {
//   constructor(private  appService:AppService){
//     super();
//   }
//   connect():Observable<User[]>
//   {
//     return this.appService.getUserList();
//   }
//   disconnect(){}




