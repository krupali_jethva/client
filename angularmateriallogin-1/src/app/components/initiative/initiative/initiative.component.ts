import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { AddRole } from 'src/app/model/add-role';

declare const myTest: any;

@Component({
  selector: 'app-initiative',
  templateUrl: './initiative.component.html',
  styleUrls: ['./initiative.component.css']

})
export class InitiativeComponent implements OnInit {
  constructor(private userService: UserService) { }
  roleList: AddRole[];

  ngOnInit() {
    this.onRoleList();
  }
  onRoleList() {
    this.userService.getRoleList().subscribe(data => {
      this.roleList = data;
      //console.log(this.roleList);
      //  myTest(this.roleList);
    });
  }
  title = 'Angular Tutorial';

  onClick() {
    myTest(this.roleList);
  }
}
