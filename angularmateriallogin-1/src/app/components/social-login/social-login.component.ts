import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Socialusers } from 'src/app/model/socialusers';
import { SocialloginService } from 'src/app/service/sociallogin-service';
import {GoogleLoginProvider,FacebookLoginProvider, AuthService} from 'ng4-social-login';
declare const githubSignInJS:any;
@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.css']
})
export class SocialLoginComponent implements OnInit {
  socialusers=new Socialusers();  
  response;
  constructor(public OAuth: AuthService,private router: Router,private SocialloginService:SocialloginService) { }

  ngOnInit() {
  }
  public socialSignIn(socialProvider: string) {  
    let socialPlatformProvider;  
    if (socialProvider === 'facebook') {  
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;  
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID; 
    }
  
      this.OAuth.signIn(socialPlatformProvider).then(socialusers => { 
      console.log(socialProvider, socialusers);  
      console.log(socialusers); 
      //this.Savesresponse(socialusers);      
    }); }
    public githubSignInTS(socialProvider: string) {  

    if(socialProvider ==='github')
    {
      githubSignInJS();
    }  
  }
  Savesresponse(socialusers: Socialusers) { 

     this.SocialloginService.Savesresponse(socialusers).subscribe((res: any) => {  

      debugger;  

      console.log(res);  

      this.socialusers=res;  

      this.response = res.userDetail;  

      localStorage.setItem('socialusers', JSON.stringify( this.socialusers));  

      console.log(localStorage.setItem('socialusers', JSON.stringify(this.socialusers)));  

      this.router.navigate([`/users`]);  

    })  

  }    
}
