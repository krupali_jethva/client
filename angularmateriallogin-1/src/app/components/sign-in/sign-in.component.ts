import { Component, OnInit } from '@angular/core';
import { AuthFireBaseService } from 'src/app/service/firebase/auth-fire-base.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(    public authService: AuthFireBaseService) { }

  ngOnInit() {
  }

}
