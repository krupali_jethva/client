import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/model/user';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { AddRole } from 'src/app/model/add-role';
import { SiteMaster } from 'src/app/model/site-master';
import { UserStatusMaster } from 'src/app/model/status-master';
import { CaptchaModel } from 'src/app/model/captcha-model';
//https://www.javatpoint.com/angular-spring-file-upload-example
//https://w3path.com/new-angular-8-file-upload-or-image-upload/

export interface Status {
  value: string;
  viewValue: string;
}

export interface Role {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  url = null;
  registerForm: FormGroup;
  selectedFiles: FileList;
  currentFileUpload: File;

  user: User;
  captchaModel: CaptchaModel;
  roleMaster: AddRole;
  statusMaster: UserStatusMaster;

  roleList: AddRole[];
  statusList: UserStatusMaster[];
  isSuperAdmin: boolean;
  submitMessage: string = "";

  constructor(private fb: FormBuilder, private appService: AppService, private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.user = new User();
    this.captchaModel = new CaptchaModel();
    this.userService.getCaptcha().subscribe(
      (result) => this.captchaModel = result,
      (error) => console.log(error),
    );
    console.log(this.captchaModel);
    // create the form object.  
    this.registerForm = this.fb.group({
      captchaEnter: ['', Validators.required],
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      profileImage: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required),
      roleMaster: new FormControl('', Validators.required),
      statusMaster: new FormControl('', Validators.required),
      isSuperAdmin: new FormControl()
    }, {
      validator: MustMatch('password', 'confirmPassword')
    }
    );

    this.onRoleList();
    this.onStatusList();
  }


  onRoleList() {
    this.userService.getRoleList().subscribe(data => {
      this.roleList = data;
      console.log(data);
    });
  }
  onStatusList() {
    this.userService.getStatusList().subscribe(data => {
      this.statusList = data;
      console.log(data);
    });
  }
  onSubmit() {
    if (this.registerForm.invalid) {
      return;
    }
    this.user.captchCode = this.registerForm.controls['captchaEnter'].value;
    if (this.captchaModel.captchaCode == +this.user.captchCode) {
      this.getUserFormData();
      this.appService.createUser(this.user)
      .subscribe(
        data => {
          let result = data.json();
          if (result != null) {
            if (this.selectedFiles != null) {
              this.currentFileUpload = this.selectedFiles.item(0);
              console.log("this.currentFileUpload :: " + this.currentFileUpload);
              this.appService.uploadFile(this.currentFileUpload, result).subscribe(
                res => {
                  let re = res.json();
                  if (re > 0) {
                    this.submitMessage="file upload successfully";                
                    this.registerForm.reset;
                    this.gotoList();
                  }
                  else {
                    this.submitMessage="error while uploading fie details";
                  }
                },
                err => {
                  this.submitMessage="error while uploading fie details";
                }
              );
            }
          }
        },
        error => console.log(error));
    } else {
      this.submitMessage = "Please enter correct captcha code.";
    }


   
  }
  getUserFormData() {
    this.user.firstName = this.registerForm.controls[""].value;
    this.user.lastName = this.registerForm.controls[""].value;
    this.user.username = this.registerForm.controls[""].value;
    this.user.password = this.registerForm.controls[""].value;
    this.user.email = this.registerForm.controls[""].value;
    this.user.profileImage = this.registerForm.controls[""].value;
    this.user.roleMaster = this.registerForm.controls[""].value;
    this.user.statusMaster = this.registerForm.controls[""].value;
    this.user.isSuperAdmin = this.registerForm.controls[""].value;
  }



  preview() {
    // Show preview 
    var reader = new FileReader();
    reader.readAsDataURL(this.currentFileUpload);
    // reader.onload = (_event) => {
    //   this.previewUrl = reader.result;
    // }
    reader.onload = (_event) => { // called once readAsDataURL is completed
      this.url = reader.result;
    }
  }
  public delete() {
    this.url = null;
  }
  fileProgress(fileInput: any) {
    this.currentFileUpload = <File>fileInput.target.files[0];
    if (this.currentFileUpload.type.match('image.*')) {
      var size = this.currentFileUpload.size;
      if (size > 1000000) {
        alert("size must not exceeds 1 MB");
        this.registerForm.get('profileImage').setValue("");
      }
      else {
        this.selectedFiles = fileInput.target.files;
        //this.selectedFiles = event.target.files;  
        this.preview();
      }
    } else {
      alert('invalid format!');
      this.registerForm.get('profileImage').setValue("");
    }

  }
  gotoList() {
    this.router.navigate(['/users']);
  }
  onGroupsChange(selectedPizzas: string[]) {
    console.log(selectedPizzas);
  }

}
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}