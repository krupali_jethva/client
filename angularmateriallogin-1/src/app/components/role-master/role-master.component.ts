import { Component, OnInit } from '@angular/core';
import { AddRole } from 'src/app/model/add-role';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { SiteMaster } from 'src/app/model/site-master';

export interface RoleModel {
  index: number;
  id: number;
  roleName: string;
  roleDescription: string;
  roleOrder: number;
}

@Component({
  selector: 'app-role-master',
  templateUrl: './role-master.component.html',
  styleUrls: ['./role-master.component.css']
})
export class RoleMasterComponent implements OnInit {

  addRole: AddRole = new AddRole();
  siteId: SiteMaster = new SiteMaster();

  isEdit = false;

  constructor(private fb: FormBuilder, private userService: UserService,private router:Router) {
    this.addRole = new AddRole();
    this.addRole.siteId=new SiteMaster();
    this.addRole.siteId.id = 1 ;
    this.isEdit = false;

  }

  roleMasterForm: FormGroup;
  roles: Array<any>;
  roleId: number;

  // by backend
  data: RoleModel[] = [];
  displayedColumnsName: string[] = ['id', 'roleName', 'roleDescription', 'roleOrder', 'action'];
  roleDataSource;


  ngOnInit() {
    this.roleMasterForm = this.fb.group({
      roleName: new FormControl('', Validators.required),
      roleDescription: new FormControl('', Validators.required),
      roleOrder: new FormControl('', Validators.required),
      id: new FormControl(),
      siteId:new FormControl()
    });
    this.reloadData();
  }
  reloadData() {
    this.userService.getRoleList().subscribe(data => {
      this.roles = data;
      this.roleDataSource = new MatTableDataSource(data);
      console.log(data);
    });
  }
  onEdit(userId: number) {
    this.userService.roleEdit(userId).subscribe(dataResult => {
      this.addRole = dataResult;
      console.log(this.addRole);
    }, (error) => console.log(error));
    this.isEdit = true;
  }
  onSubmitRole() {
    this.userService.saveRole(this.addRole)
      .subscribe(
        data => {
          let result = data;
          this.reloadData();
          //this.roleMasterForm.reset();
          console.log(result);
        },
        error => console.log(error));
  }
  onUpdateRole() {
    this.userService.roleUpdate(this.addRole)
    .subscribe(
      dataResult => {
        this.reloadData();
        this.roleMasterForm.reset();
        if (dataResult) {
            this.isEdit = false;    
      }
      },
      (error) => console.log(error),
    );
  }
  onDeleteRole(roleId:number){
    this.userService.roleDelete(roleId).subscribe(
      (result)=>{
        this.reloadData();
        this.roleMasterForm.reset();
      },
      (error)=>console.log(error),
    );
  }

  onCancelSave() {
    //this.isEdit = false;
    // this.ngOnInit();
    this.roleMasterForm.reset();
  }

  onCancelEdit() {
    this.isEdit = false;
    // this.ngOnInit();
    this.roleMasterForm.reset();
    //this.router.navigate(['']);
  }
}
