import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, RequestOptions , Headers } from '@angular/http';  
import { User } from './model/user';

@Injectable({
  providedIn: 'root'
})

export class AppService {
   private baseUrl: string;
   private attachementUrl: string;
 
  constructor(private http: Http) {
     this.baseUrl = 'http://localhost:8080/admin/';
     this.attachementUrl='http://localhost:8080/attachement/';
   }
  
   createUser(User : any):Observable<any>
   {
    console.log(this.http.post(this.baseUrl,User));
    return this.http.post(this.baseUrl+"saveUser",User);
   }

   getUserList(): Observable<any> {
    return this.http.get(this.baseUrl+"users");
  }

  uploadFile(file:File,user :User) : Observable<any>  
  {  
    console.log("in service id :: "+user.id+" :: file:: "+file );
    let url = this.attachementUrl + "uploadImage/" + user.id ;    
    const formdata: FormData = new FormData();      
    formdata.append('file', file);     
    return this.http.post(url , formdata);  
  } 

  // get(searchTerm) {
  //   const apiLink = this.baseUrl+"users/" + searchTerm;
  //   return this.http.get(apiLink).pipe(map((response: any) => {
  //     if (response.length > 0) {
  //       let profileImageUrl = response[0].profileImage ;
  //       console.log("profileImageUrl :: "+profileImageUrl);
  //       return response[0].profileImage;
  //     } else {
  //       return 'https://media.giphy.com/media/YaOxRsmrv9IeA/giphy.gif'; // dancing cat for 404
  //     }
  //   }));
  // }
}
